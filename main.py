#!/usr/bin/env python
# -*- coding: utf-8 -*-
# This program is dedicated to the public domain under the CC0 license.

"""
Simple Bot that retrives the tv programm 
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""

import logging

import telegram
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from tvguide import get_programm_from_msg, get_link_from_msg

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)


def start(update, context):
    """Send a message when the command /start is issued."""
    update.message.reply_text('Hi!')


def help(update, context):
    """jSend a message when the command /help is issued."""

    text = """
Hi, I am your *TvGuide*\! You can ask for the tv programm with commands like:

\- `Was läuft morgen bei Ard`
\- `Gibt mir das Programm von Pro 7`
\- `Was ist das tv programm am 12.02 bei zdf neo`

If you find any buggs send me an email at [simon\.heinrich@iesy\.net](simon.heinrich@iesy.net)
"""
    update.message.reply_text(text, parse_mode=telegram.ParseMode.MARKDOWN_V2) #,  parse_mode=telegram.ParseMode.MARKDOWN_V2)



def reply(update, context):

    bot = update.message.bot
    chat_id = update.message.chat_id
    bot.send_chat_action(chat_id=chat_id, action=telegram.ChatAction.TYPING)

    msg = update.message.text
    reply_text = get_programm_from_msg(msg)
    update.message.reply_text(reply_text, parse_mode=telegram.ParseMode.MARKDOWN_V2)

    reply_link = get_link_from_msg(msg)
    update.message.reply_text(reply_link, parse_mode=telegram.ParseMode.MARKDOWN_V2)


def error(update, context):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, context.error)


def main():
    """Start the bot."""
    # Create the Updater and pass it your bot's token.
    updater = Updater("1231185454:AAGO-8UoKZfgrRzjBT6Db077UKhXYH1_9H8", use_context=True)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", help))
    dp.add_handler(CommandHandler("help", help))

    # on noncommand i.e message - echo the message on Telegram
    dp.add_handler(MessageHandler(Filters.text, reply))

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    print("Starting the telegram server...")
    main()
