import pandas as pd
import re
import urllib.request
import datetime
from bs4 import BeautifulSoup

CHANNEL_ALIAS = {
    "ARD": ["ard", "erste"],
    "2NEO": ["2neo", "zdfneo", "zdf neo"],
    "ZDF": ["zdf", "zweite"],
    "RTL2": ["rtl2", "rtl 2"],
    "RTL": ["rtl"],
    "ARTE": ["arte"],
    "VOX": ["vox"],
    "SAT1": ["sat1", "sat.1", "sat 1"],
    "PRO7": ["pro7", "pro.7", "pro 7"],
    "K1": ["kabel 1", "kabel1", "k1"],
}

REL_DATE_ALIAS = {
    0: ["heute"],
    1: ["morgen"],
    2: ["übermorgen"]
}

WEEK_DAY_ALIAS = {
    0: ["montag", "mon"],
    1: ["dienstag"],
    2: ["mittwoch"],
    3: ["donnerstag", "don"],
    4: ["freitag"],
    5: ["samstag", "sam"],
    6: ["sonntag", "son"],
}


def get_programm_as_df(channel, date):
    page = 0
    programm_df = pd.DataFrame()
    while True:
        try:
            # request html document
            page += 1
            url = f"https://www.tvspielfilm.de/tv-programm/sendungen/?page={page}&date={date}&channel={channel}"
            with urllib.request.urlopen(url) as fp:
                html_doc = fp.read().decode("utf8")

            # find programm table
            soup = BeautifulSoup(html_doc, 'html.parser')
            programm_html_table = str(soup.findAll("table", {"class": "info-table"})[0])

            # parse into pandas table
            page_df = pd.read_html(programm_html_table)[0]

            # Parse Date correctly
            page_df.Zeit = pd.to_datetime(
                page_df.Zeit.apply(lambda x: x[:5] + "_" + date),
                format="%H:%M_%Y-%m-%d"
            )
            programm_df = programm_df.append(page_df)
        except Exception:
            break
    return programm_df


def programm_df_to_text(df):
    full_text = []
    for index, row in df.iterrows():
        time = row.Zeit.strftime("%H:%M")
        title = row.Titel
        text = f"*{time}* \- {title}"
        text = re.sub(r"\d{4}", "", text)
        text = re.sub(r"\s[A-Z/]+\s*$", "", text)
        full_text.append(text)
    return "\n".join(full_text)


def parse_slot(msg, id2alias):
    msg = msg.lower()
    for slot_id, alias in id2alias.items():
        if [a for a in alias if a in msg]:
            return slot_id


def next_weekday(d, weekday):
    days_ahead = weekday - d.weekday()
    if days_ahead <= 0:  # Target day already happened this week
        days_ahead += 7
    return d + datetime.timedelta(days_ahead)

def find_date(msg):
    date_pattern = r"(?P<day>\d{1,2})[\.\-\/](?P<month>\d{1,2})[\.\-\/]?(?P<year>\d{4}|\d{2})?"
    search_result = re.search(date_pattern, msg)

    if search_result:
        res_dict = search_result.groupdict()
        raw_day = res_dict["day"]
        raw_month = res_dict["month"]
        raw_year = res_dict["year"] if res_dict["year"] else str(datetime.date.today().year)

        day = "0"+raw_day if len(raw_day) == 1 else raw_day
        month = "0"+raw_month if len(raw_month) == 1 else raw_month
        year = "20"+raw_year if len(raw_year) == 2 else raw_year

        date = f"{year}-{month}-{day}"

        return date


def parse_msg(msg):
    channel = parse_slot(msg, CHANNEL_ALIAS)
    if not channel:
        raise ValueError("No channel")

    week_day = parse_slot(msg, WEEK_DAY_ALIAS)
    rel_date = parse_slot(msg, REL_DATE_ALIAS)
    if week_day is not None:
        date = next_weekday(datetime.date.today(), week_day).strftime("%Y-%m-%d")
    elif rel_date is not None:
        date = (datetime.date.today() + datetime.timedelta(days=rel_date)).strftime("%Y-%m-%d")
    elif find_date(msg):
        date = find_date(msg)
    else:
        date = datetime.date.today().strftime("%Y-%m-%d")
    return channel, date

def escape_symbols_in_msg(msg):
    msg = msg.replace("!", "\!")
    msg = msg.replace(".", "\.")
    msg = msg.replace("(", "\(")
    msg = msg.replace(")", "\)")
    msg = msg.replace("=", "")
    msg = msg.replace("-", "")
    return msg

def get_link_from_msg(msg):
    channel, date = parse_msg(msg)
    url = f"https://www.tvspielfilm.de/tv-programm/sendungen/?page=1&date={date}&channel={channel}"
    return f"[TV Programm of {channel}]({url})"

def get_programm_from_msg(msg):
    reply_msg = programm_df_to_text(get_programm_as_df(*parse_msg(msg)))
    return escape_symbols_in_msg(reply_msg)
